/*Supermercado RR*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// Estructura de un producto
typedef struct Producto
{
  int codigo_producto;
  char descripcion[20];
  float precio_unitario;
  struct Producto *siguiente;

} t_producto;

float aplicar_igv(float);
void emitir_saludo(void);
void imprimir_producto(t_producto);
t_producto *leer_producto(void);
void eliminar_todo(t_producto *);

int main(int argc, char const *argv[])
{
  t_producto *primer = NULL, *ultimo = NULL;
  t_producto *producto;
  char decision;
  int i, cantidad = 0;
  float total = 0.0;

  emitir_saludo();

  while (1)
  {
    do
    {
      printf("Hay %d productos en el carrito. Pasamos otro? [s/n]", cantidad);
      decision = getchar();
      while (getchar() == EOF)
        ;

    } while (decision != 's' && decision != 'S' && decision != 'n' && decision != 'N');
    if (decision == 'n' || decision == 'N')
      break;

    producto = leer_producto();
    if (primer == NULL)
      primer = producto;
    if (ultimo != NULL)
      ultimo->siguiente = producto;
    ultimo = producto;
    cantidad++;
  }

  for (producto = primer; producto != NULL; producto = producto->siguiente)
  {
    imprimir_producto(*producto);
    total += producto->precio_unitario;
  }

  printf("Total productos: %d\n", cantidad);
  printf("Precio total sin IGV: %.2f\n", total);
  printf("Precio total con IGV: %.2f\n", aplicar_igv(total));
  printf("\n\nBuenos dias.\n");

  eliminar_todo(primer);

  return 0;
}

float aplicar_igv(float precio_base)
{
  return precio_base * 1.18;
}

void emitir_saludo()
{
  printf("****************************************\n");
  printf("***            SUPERMERCADOS         ***\n");
  printf("**Porque la calidad, no es lo primero **\n");
  printf("****************************************\n");
}

void imprimir_producto(t_producto t)
{
  printf("%10d %-20s\t%.2f\n", t.codigo_producto, t.descripcion, t.precio_unitario);
}

t_producto *leer_producto()
{
  t_producto *p = malloc(sizeof(t_producto));
  char entrada[80];

  printf("Codigo de producto: ");
  fgets(entrada, 10, stdin);
  if (entrada[strlen(entrada) - 1] == '\n')
    entrada[strlen(entrada) - 1] = '\0';

  p->codigo_producto = (int)strtol(entrada, NULL, 10);

  printf("Descripcion: ");
  fgets(p->descripcion, 20, stdin);
  if (p->descripcion[strlen(p->descripcion) - 1] == '\n')
    p->descripcion[strlen(p->descripcion) - 1] == '\0';

  printf("Precio: ");
  fgets(entrada, 10, stdin);
  if (entrada[strlen(entrada) - 1] == '\n')
    entrada[strlen(entrada) - 1] == '\0';

  p->precio_unitario = strtol(entrada, NULL, 10);

  p->siguiente = NULL;

  return p;
}

void eliminar_todo(t_producto *producto)
{
  t_producto *este = producto, *siguiente;
  while (este != NULL)
  {
    siguiente = este->siguiente;
    este->siguiente = NULL;
    free(este);
    este = siguiente;
  }
}
